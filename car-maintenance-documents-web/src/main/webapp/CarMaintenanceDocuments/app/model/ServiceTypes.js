Ext.define('CarMaintenanceDocuments.model.ServiceTypes', {
    extend : 'Ext.data.Model',

    fields : [
        
        {
            name: 'serviceId'
        },
        {
            name: 'serviceName'
        }
  
    ]

});