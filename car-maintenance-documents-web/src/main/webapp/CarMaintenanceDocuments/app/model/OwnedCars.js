Ext.define('CarMaintenanceDocuments.model.OwnedCars', {
    extend : 'Ext.data.Model',

    fields : [
        
        {
            name: 'manufacturerName'
        },
        {
            name: 'typeName'
        },
        {
            name: 'buildDate'
        }
  
    ]

});